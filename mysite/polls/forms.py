from django import forms


class LoginForm(forms.Form):
    """ Validate login form. """
    username = forms.CharField(label='username', max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)


class RegistrationForm(forms.Form):
    """ Validate registration form. """
    new_username = forms.CharField(label='username', max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)
    password_again = forms.CharField(max_length=50, widget=forms.PasswordInput)
