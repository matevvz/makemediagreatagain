from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .models import FuelStation, Evaluation, OpeningHours
from .forms import LoginForm, RegistrationForm


SKLANJANJE = {
    'Ljubljana': 'Ljubljani',
    'Maribor': 'Mariboru',
    'Novo mesto': 'Novem mestu',
}


def index(request):
    # latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', {})


def about(request):
    return render(request, 'polls/about.html', {})


def detail(request, fuel_station_id):
    fuel_station = get_object_or_404(FuelStation, pk=fuel_station_id)
    prices = [(smh + '-oktanski' if smh != 'diesel' else 'Dizel',
               "%.3f" % fuel_station.get_fuel_price(smh))
              for smh in ('95', '98', '100', 'diesel')
              if fuel_station.get_fuel_price(smh) > 0]

    return render(request,
                  'polls/detail.html',
                  {'fuel_station': fuel_station,
                   'prices': prices})


def advanced_search(request, location='Ljubljana', fuel_type='95'):
    fuel_stations_list = FuelStation.objects.filter(location=location)

    fuel_stations_list = sorted([smh for smh in fuel_stations_list
                                 if smh.get_fuel_price(fuel_type) > 0],
                                key=lambda x: x.get_fuel_price(fuel_type),
                                reverse=False)

    avg_price = (sum([smh.get_fuel_price(fuel_type) for smh in
                      fuel_stations_list]) * 1.0 / len(fuel_stations_list)
                 if fuel_stations_list else '-')

    lowest_price = (min(smh.get_fuel_price(fuel_type)
                        for smh in fuel_stations_list)
                    if fuel_stations_list else '-')

    context = {'fuel_stations_list': fuel_stations_list,
               'location': SKLANJANJE.get(location, 'Ljubljani'),
               'fuel_type': fuel_type,
               'avg_price': "%.3f" % avg_price if
               type(avg_price) == float else avg_price,
               'lowest_price': "%.3f" % lowest_price if
               type(lowest_price) == float else lowest_price}

    return render(request, 'polls/advanced_search.html', context)


def search_location(request):
    args = ()
    if request.method == "POST":
        location = request.POST['t_location'] or 'Ljubljana'
        fuel_type = request.POST['t_fuel_type'].split('-')[0] or '95'
        args = (location, fuel_type)
    return HttpResponseRedirect(reverse('polls:advanced_search',
                                        args=args))


def route_planner(request):
    return render(request, 'polls/route_planner.html', {})


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('polls:index', args=()))


def login_user(request):
    if request.method == "POST":
        if 'new_username' in request.POST:
            # registration
            form = RegistrationForm(request.POST)
            if form.is_valid():
                user = User.objects.create_user(
                    username=form.cleaned_data['new_username'],
                    password=form.cleaned_data['password'])
            else:
                return render(request, 'polls/login_user.html',
                              {'error_msg': 'Registration failed. Try again!'})

        else:
            # existing user login
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password'])
            else:
                return render(request, 'polls/login_user.html',
                              {'error_msg': 'Login failed. Try again!'})

        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('polls:index', args=()))
        else:
            return render(request, 'polls/login_user.html',
                          {'error_msg': 'Wrong credentials. Try again!'})

    return render(request, 'polls/login_user.html', {})
