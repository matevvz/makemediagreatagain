import datetime

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

WEEKDAYS = [
  (1, _("Monday")),
  (2, _("Tuesday")),
  (3, _("Wednesday")),
  (4, _("Thursday")),
  (5, _("Friday")),
  (6, _("Saturday")),
  (7, _("Sunday")),
]


class FuelStation(models.Model):
    """ A model which defines a single fuel station. """
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    long = models.DecimalField(max_digits=9, decimal_places=6, default='0.0')
    lang = models.DecimalField(max_digits=9, decimal_places=6, default='0.0')
    rating = models.FloatField('avg. rating', default=-1.0)

    price_95 = models.FloatField('95 octane fuel price', default=-1.0)
    price_98 = models.FloatField('98 octane fuel price', default=-1.0)
    price_100 = models.FloatField('100 octane fuel price', default=-1.0)
    price_diesel = models.FloatField('diesel fuel price', default=-1.0)

    def get_fuel_price(self, fuel_type='95'):
        if fuel_type == '95':
            return self.price_95
        if fuel_type == '100':
            return self.price_100
        if fuel_type == '98':
            return self.price_98
        return self.price_diesel

    def __str__(self):
        return self.name + ", " + self.address


class OpeningHours(models.Model):
    fuel_station_id = models.ForeignKey(FuelStation)
    weekday_from = models.IntegerField(choices=WEEKDAYS, unique=True)
    weekday_to = models.IntegerField(choices=WEEKDAYS)
    from_hour = models.TimeField()
    to_hour = models.TimeField()

    def get_weekday_from_display(self):
        return WEEKDAYS[self.weekday_from]

    def get_weekday_to_display(self):
        return WEEKDAYS[self.weekday_to]


class Evaluation(models.Model):
    comment = models.TextField()
    rating = models.IntegerField()
    pub_date = models.DateTimeField('date published')
    fuel_station_id = models.ForeignKey(FuelStation)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    # When the referenced object is deleted, also delete the objects
    # that have references to it

    class Meta:
        permissions = (
            ('edit_evaluation', 'Can remove evaluation/comment.'),
        )

    def __str__(self):
        return '{} {} {}'.format(self.rating, self.user_id,
                                 self.fuel_station_id)


@receiver(post_delete)
def update_rating(sender, instance=None, created=False, **kwargs):
    """ Listen & Trigger upon a rating change (deletion, insertion). """
    if sender.__name__ in ('Evaluation', ) and instance is not None:
        fuel_station = instance.fuel_station_id

        evaluations = Evaluation.objects.filter(
            fuel_station_id=fuel_station.id)

        fuel_station.rating = (sum([smh.rating for smh in evaluations])
                               * 1.0 / len(evaluations))

        fuel_station.save()


@receiver(post_save)
def another_update_rating(sender, instance=None, created=False, **kwargs):
    """ Listen & Trigger upon a rating change (deletion, insertion). """
    if sender.__name__ in ('Evaluation', ) and instance is not None:
        fuel_station = instance.fuel_station_id

        evaluations = Evaluation.objects.filter(
            fuel_station_id=fuel_station.id)

        fuel_station.rating = (sum([smh.rating for smh in evaluations])
                               * 1.0 / len(evaluations))

        fuel_station.save()
