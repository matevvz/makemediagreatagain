from django.conf.urls import url

from . import views

""" Django starts at the first regular expression and makes
its way down the list, comparing the requested URL against
each regular expression until it finds one that matches. """

app_name = 'polls'
urlpatterns = [
    # index
    url(r'^index/$', views.index, name='index'),

    # logout
    url(r'^odjava/$', views.logout_user, name='logout_user'),

    # login
    url(r'^prijava/$', views.login_user, name='login_user'),

    # about us
    url(r'^o_nas/$', views.about, name='about'),

    # fuel station detail
    url(r'^crpalka/(?P<fuel_station_id>[0-9]+)/$', views.detail, name='detail'),

    # advanced search
    url(r'^napredno_iskanje/(?P<location>[a-zA-ZžŽčČšŠ]+( [a-zA-ZžŽčČšŠ]+)*)/(?P<fuel_type>[0-9a-zA-Z]+)/$',
        views.advanced_search, name='advanced_search'),
    url(r'^napredno_iskanje/$', views.advanced_search, name='advanced_search'),

    # trip planner
    url(r'^nacrtovalec_poti/$', views.route_planner, name='route_planner'),

    # search location
    url(r'^search_location/$', views.search_location, name='search_location'),
]
