function vstavi() {
    table = document.getElementById("tabelca");
    ind = table.rows.length - 1;
    row = table.insertRow(ind);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    cell1.innerHTML = "1,209";
    cell2.innerHTML = '<span>OMV</span><br/>Večna pot 113';
    cell3.innerHTML = "Osrednjeslovenska regija";
    cell4.innerHTML = "20s nazaj";
}

function validate_poraba() {
    var x = document.forms["vnos-nacrtuj"]["povprecna-poraba"].value;
    if (x == "") {
        alert("Polje je prazno.");
        return false;
    }
    else if (!(/^[0-9,.]*$/.test(x))){
        alert("Vpiši povprečno uporabo, npr. 6,7.");
        return false;
    }
}
