# README **updated** #

It has been recommended in the description of the first project phase to consider an idea that can be brought to market. For this reason, the title of the topic was modified at the eleventh hour:

*A web application to monitor fuel prices in a free-market system*

Created to offer users a clear and easily accessible overview of fuel prices.

* Conscious & fair pricing policies are emphasised on the map,
* built-in tools help users calculate effective costs and save money,
* filling stations can be rated and comments can be up/down -voted.
* Google maps support, languages: SLO

###### The web application covers/monitors fuel prices in [Slovenia](https://en.wikipedia.org/wiki/Slovenia).

# Target audience #

{Tesla drivers}^C. Everyone who wants to save money on fuel.

# Supported devices #

Mobile and desktop browsers alike.

# Poročilo o težavah #

Trenutno se ukvarjam z nastavitvijo pridobivanja lokacije uporabnika. Element je v implementaciji sicer vključen, vendar so potrebni popravki oziorma izboljšave.

# Izpostavljena gradnika #

Še najbolj mi je všeč oblikovanje strani, razširjanje tabele s pomočjo javascript funkcije in možnost načrtovanja poti (sicer backend razširitev).

# Before #

~~This is an attempt to conceptualise a self-balancing online media outlet (similar to online newspapers, but not quite), relying on various support mechanisms to encourage and promote fact-based, neutral content creation from novice and experienced reporters alike. If media is privately owned, can it be truly independent? We want to provide an open-source solution that is fully in the public domain to provide absolute transparency and objective reporting. Too often media appears biased, commonly chasing insignificant topics that maximise advertising profits rather than help shape a more conscious, informed public attitude towards real issues. Let's make media great again.~~